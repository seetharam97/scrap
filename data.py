import flask
from flask_api import status
import pandas as pd
from flask import request, jsonify, make_response
from werkzeug.wrappers import Request, Response


app = flask.Flask(__name__)
app.config['DEBUG'] = True

@app.route('/excel', methods=['POST'])
def excel():
	args = request.get_json()
	url = args.get("url")
	path = args.get("path")
	file_name = args.get("file_name")
	tbl_num = args.get("tbl_num")
	table = pd.read_html(url)[tbl_num]
	table.to_excel(path+file_name+".xlsx")
	response = make_response(jsonify({"message": "excel successfully created", "status": status.HTTP_201_CREATED }))
	return response

if __name__ ==('__main__'):
	from werkzeug.serving import run_simple
	run_simple('localhost', 8080, app)

